package Util;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.testng.Assert;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ExcelWriterHelper {
    private ExcelWriter excelWriter;
    private static int stepCount=0;

    public void start() throws WriteException, IOException, BiffException, IOException {
        excelWriter=new ExcelWriter();
        excelWriter.createWorkBook();

    }
    public void end(String[] testCaseDetails ) throws RowsExceededException, WriteException, IOException{
        String[] message={"END OF TESTCASE"};
        excelWriter.toDetailedReportPage(message);
        excelWriter.toSummaryPage(testCaseDetails);
        excelWriter.closeExcel();
        stepCount=0;//needed for sequential execution
    }

//    public void toDetailedReportSheet(String stepdescription,int passFlag) throws RowsExceededException, WriteException, IOException{
//        CountersForExcel.totalStepCount++;
//        //ITestResult result;
//        stepCount++;
//
//        String contentsToExcel[]={CountersForExcel.testCaseName,CountersForExcel.testModuleName,"Step "+stepCount,stepdescription,"",""};
//
//        if(passFlag==1){
//            CountersForExcel.passStepCount++;
//            contentsToExcel[4]="PASS";
//            contentsToExcel[5]=fnChangeDateFormat();//current execution time
//            excelWriter.createContent(1,contentsToExcel);
//
//        }
//        else{
//         //   CountersForExcel.testCaseResult="FAIL";
//        //    CountersForExcel.failStepCount++;
//            contentsToExcel[4]="FAIL";
//            contentsToExcel[5]=fnChangeDateFormat();//current execution time
//            excelWriter.createContent(1,contentsToExcel);
//            Assert.fail("Element not found Exception");
//        }
//

//    }
//    public void toReportExcelCell(int sheetNo,int column, int row, String s) throws RowsExceededException, WriteException, FileNotFoundException {
//
//        excelWriter.addContentToCell(sheetNo, column, row, s);
//    }
    public void addStepExplicitlyInReport(String s) throws RowsExceededException, WriteException{
        excelWriter.addStepExplicitlyInReport( s);
    }

//    public String fnChangeDateFormat(){
//
//        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
//        Date dateobj = new Date();
//        return df.format(dateobj);
//    }


}



