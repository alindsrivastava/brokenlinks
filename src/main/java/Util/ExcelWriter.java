package Util;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import jxl.*;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.Sheet;


public class ExcelWriter {

    /**
     * @param args
     * @throws IOException
     * @throws WriteException
     */
    private static WritableCellFormat timesBoldUnderline ;// fontColorForPass,
           // fontColorForFail;
    private static WritableWorkbook workbook;
    public static long lStartTime, lEndTime;
    private static WritableCellFormat times;
    public String FILE_NAME=System.getProperty("user.dir")+"/src/main/resources/ExecutionReport.xlsx";

    public static int rowCount_SummaryPage = 0,
            rowCount_DetailedReportPage = 1, rowCount_AddingDetails_Sheet2 = 1,rowcountforapplicationid=1,rowcountforapp=1;

    // private static WritableWorkbook workbook;

    public void createWorkBook() throws IOException, WriteException, BiffException {


        // /////////////////////////////////////////////////////////////////////////////////
        // Lets create a times font
        WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
        WritableFont pass = new WritableFont(WritableFont.TIMES, 10);
        WritableFont fail = new WritableFont(WritableFont.TIMES, 10);
     //   pass.setColour(Colour.GREEN);
     //   fail.setColour(Colour.RED);

        // Define the cell format
        times = new WritableCellFormat(times10pt);
   //     fontColorForPass = new WritableCellFormat(pass);
   //     fontColorForFail = new WritableCellFormat(fail);

        // create a bold font with underlines
        WritableFont times10ptBoldUnderline = new WritableFont(
                WritableFont.TIMES, 10, WritableFont.BOLD, false,
                UnderlineStyle.SINGLE,Colour.DARK_BLUE);
        timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);

        times.setWrap(true);
  //      fontColorForPass.setWrap(true);
  //      fontColorForFail.setWrap(true);
        timesBoldUnderline.setWrap(true);

        CellView cv = new CellView();
        cv.setFormat(times);
        cv.setFormat(timesBoldUnderline);


        // /////////////////////////////////////////////////////////////////////////////////
        checkExecutionReportpresentOnPath();

        if (CountersForExcel.flagExecutionReportPresentonFolder) {
            createTempExecutionReport();
            appendExecutionReport();
        }
        else {

            //customized-----To add the application No to the detailed report sheet
      //      CountersForExcel.detailedSheetTestCaseStart_RowCounts=rowCount_DetailedReportPage;

            //File file = new File("D:/Navya Work/Mywork/maven/ICICI_POC/src/com/tcs/Report/TestExecutionReport.xls");
            File file = new File(CountersForExcel.executionReportPath);
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setLocale(new Locale("en", "EN"));
            workbook = Workbook.createWorkbook(file, wbSettings);

            workbook.createSheet("Summary", 0);
            WritableSheet excelSheet = workbook.getSheet(0);
            String headings[] = { "Sr.no ", "Broken Link Url", "Status",
                    "Date and Time of Execution", "Elapsed Time" };
            createCaption(excelSheet, headings);

            workbook.createSheet("DetailedReport", 1);
            WritableSheet excelSheet2 = workbook.getSheet(1);
            String headings1[] = { "Sr.no", "Broken Link Url", "Status",
                    "Date and Time of Execution", "Elapsed Time" };
            createCaption(excelSheet2, headings1);
        }

    }
    public void checkExecutionReportpresentOnPath(){

        File tempFile = new File(CountersForExcel.executionReportPath);
        CountersForExcel.flagExecutionReportPresentonFolder = tempFile.exists();
    }
    public void createTempExecutionReport() throws BiffException, IOException, WriteException{

        //Create a temporary worksheet
        File f2=new File(CountersForExcel.executionReportPath);
        Workbook workbook2=Workbook.getWorkbook(f2);
        WritableWorkbook tempWorkbook=Workbook.createWorkbook(new File(CountersForExcel.tempExecutionReportPath), workbook2);

        tempWorkbook.write();
        tempWorkbook.close();


    }
    public void appendExecutionReport() throws BiffException, IOException, WriteException{
        File f2=new File(CountersForExcel.tempExecutionReportPath);
        Workbook workbook2=Workbook.getWorkbook(f2);
        workbook=Workbook.createWorkbook(new File(CountersForExcel.executionReportPath), workbook2);
        WritableSheet excelSheet1 = workbook.getSheet(0);
        WritableSheet excelSheet2 = workbook.getSheet(1);

        rowCount_SummaryPage=excelSheet1.getRows();
        rowCount_DetailedReportPage=excelSheet2.getRows();
        rowcountforapp=excelSheet2.getRows();

        //customized
        CountersForExcel.detailedSheetTestCaseStart_RowCounts=rowCount_DetailedReportPage;


		/*System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"+rowCount_DetailedReportPage);
		System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"+rowCount_AddingDetails_Sheet2);*/

        workbook2.close();
    }

    private void createCaption(WritableSheet sheet, String[] headings)
            throws WriteException {

        // Write a few headers
        int i = -1;
        for (String x : headings) {
            i++;
            addCaption(sheet, i, 0, x);
        }

    }

    private void addCaption(WritableSheet sheet, int column, int row, String s)
            throws RowsExceededException, WriteException {
        Label label;
        label = new Label(column, row, s, timesBoldUnderline);
        sheet.addCell(label);
    }

    public void createContent(int sheetNo, String[] stringArray)
            throws WriteException, RowsExceededException, IOException {

        // /////////////////////////////////////for getting the filled row
        // count.
        if (sheetNo == 0) {
            rowCount_SummaryPage++;
            WritableSheet excelSheet = workbook.getSheet(0);

            int i = -1;
            for (String x : stringArray) {
                i++;
                addContent(excelSheet, i, rowCount_SummaryPage, x);
            }
        } else if (sheetNo == 1) {
            rowCount_DetailedReportPage++;
            WritableSheet excelSheet = workbook.getSheet(1);

            int i = -1;
            for (String x : stringArray) {
                i++;
                addContent(excelSheet, i, rowCount_DetailedReportPage, x);
            }
            CountersForExcel.testCaseName="";
            CountersForExcel.testModuleName="";
        }
        // /////////////////////////////////////////

    }

    private void addContent(WritableSheet sheet, int column, int row, String s)
            throws WriteException, RowsExceededException {

        Label label;
//        if (s.equals("PASS"))
//            label = new Label(column, row, s, fontColorForPass);
//        else if (s.equals("FAIL"))
//            label = new Label(column, row, s, fontColorForFail);
//        else
            label = new Label(column, row, s, times);
    //    sheet.addCell(label);
        try {
            sheet.addCell(label);
            workbook.write();
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

      //  sheet.addCell();
    }

    public void addContentToCell(int sheetcount, int count,int status , int rowno, String s)  {
        checkExecutionReportpresentOnPath();
       // InputStream inp = new FileInputStream(FILE_NAME);
     //   Workbook wb = null;
        try {
            InputStream inp = new FileInputStream(FILE_NAME);
            org.apache.poi.ss.usermodel.Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = ((org.apache.poi.ss.usermodel.Workbook) wb).getSheetAt(0);
       //     int num = sheet.getLastRowNum();
            Row row = sheet.createRow(rowno);
            row.createCell(0).setCellValue(count);
            row.createCell(1).setCellValue(s);
            row.createCell(2).setCellValue(status);
           // row.createCell()

            // Now this Write the output to a file
            FileOutputStream fileOut = new FileOutputStream(FILE_NAME);
            ((org.apache.poi.ss.usermodel.Workbook) wb).write(fileOut);
            fileOut.close();

//            wb = (Workbook) WorkbookFactory.create(inp);
//            Sheet sheet = (Sheet) wb.getSheet(0);
//            WritableSheet sheet1=workbook.getSheet(1);
//            addContent(sheet1, column, rowno, s);
//          //  sheet.
//           // Cell[] row = sheet.getRow(rowno);
//          //  Cell.class.
//          //  row.createCell(0).setCellValue("xyz");
//          //  row.

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }

        // WritableSheet Sheet1 = workbook.getSheet(sheetcount);


    }

    public void addStepExplicitlyInReport( String s) throws RowsExceededException, WriteException {
        WritableSheet sheet1=workbook.getSheet(1);
        addContent(sheet1, 1, rowCount_AddingDetails_Sheet2, s);
    }

    // For adding application id in continous run case
    // Test method for adding application id in continous run case

    public void addApplicationid(String str)throws RowsExceededException, WriteException
    {
        WritableSheet sheet1=workbook.getSheet(1);
        addContent(sheet1, 6, rowcountforapp, str);
    }
    public void toSummaryPage(String[] testCaseDetails)
            throws RowsExceededException, WriteException, IOException {
        createContent(0, testCaseDetails);
        executionDateandTime();
        completeExecutionElapsedTime();
    }
    public void toDetailedReportPage(String[] message) throws RowsExceededException, WriteException, IOException{
        createContent(1, message);
    }
    public void executionDateandTime() throws RowsExceededException,
            WriteException {
        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Date dateobj = new Date();

        WritableSheet excelSheet = workbook.getSheet(0);
        addContent(excelSheet, 5, rowCount_SummaryPage, df.format(dateobj));

        System.out.println(df.format(dateobj));
    }


    //For whole execution time
    public void completeExecutionElapsedTime() throws RowsExceededException, WriteException{
        String remainingTime=elapsedExecutionTime(CountersForExcel.testCaseEndTime,CountersForExcel.testCaseStartTime);


        WritableSheet excelSheet2 = workbook.getSheet(0);
        addContent(excelSheet2, 6, rowCount_SummaryPage, remainingTime);
    }

    public String elapsedExecutionTime(long testCaseEndTime,long testCaseStartTime)
            throws RowsExceededException, WriteException {
        // lEndTime=new Date().getTime();
        //long difference = CountersForExcel.testCaseEndTime - CountersForExcel.testCaseStartTime; // check different
        long difference = testCaseEndTime - testCaseStartTime;
        // Date date = new Date(difference);
        // long millis = 3600000;

        String hms = String.format(
                "%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(difference),
                TimeUnit.MILLISECONDS.toMinutes(difference)
                        - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
                        .toHours(difference)),
                TimeUnit.MILLISECONDS.toSeconds(difference)
                        - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
                        .toMinutes(difference)));
        System.out.println(hms);
        return (hms);
    }

    public void closeExcel() throws WriteException, IOException {
        WritableSheet excelSheet1 = workbook.getSheet(0);
        WritableSheet excelSheet2 = workbook.getSheet(1);
        excelSheet1.setColumnView(0, 20);
        excelSheet1.setColumnView(1, 20);
        excelSheet1.setColumnView(2, 20);

        excelSheet2.setColumnView(0, 20);
        excelSheet2.setColumnView(1, 20);
        excelSheet2.setColumnView(2, 20);
        excelSheet2.setColumnView(3, 20);
        workbook.write();
        workbook.close();
    }

}
