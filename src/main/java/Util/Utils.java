package Util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Utils {

    private Properties prod = new Properties();

    public Utils(String path) {
        loadPropertyFile(path);
    }

    public void loadPropertyFile(String propertyFileName) {
        try {


            prod.load(new FileInputStream(propertyFileName));
        } catch (IOException e) {

            // Assert.fail("Unable to load file!", e);

            e.printStackTrace();
        }
    }


    public String getProperty(String propertyKey) {
        return prod.getProperty(propertyKey.trim());
    }



}
