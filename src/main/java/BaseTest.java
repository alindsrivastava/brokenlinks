import Util.Utils;
import org.apache.commons.exec.OS;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;



public class BaseTest {
String file="./src/main/resources/settings.properties";
        Utils utils = new Utils(file);
public static WebDriver aDriver=null;
        String browser=utils.getProperty("browser_name");
static String basePath=System.getProperty("user.dir")+"/src/main/resources";


@BeforeSuite
public   void setUpConfig(){
        switch(browser.toLowerCase()){

        case "firefox":
        setDriverProperty(browser);
        aDriver = new FirefoxDriver();

        break;

        case "chrome":
        setDriverProperty(browser);
        aDriver = new ChromeDriver();
        break;
default:
        System.out.println("Browser not supported");
        }


        }



private static void setDriverProperty(String driverType) {


        switch (driverType.toLowerCase()) {
        case "chrome":
        case "googlechrome":
        if (OS.isFamilyMac()) {

        System.setProperty("webdriver.chrome.driver",
        basePath+"/chromedriver_mac");
        } else if (OS.isFamilyWindows()) {
        System.setProperty("webdriver.chrome.driver",
        basePath+"/chromedriver.exe");

        }
        break;
        case "firefox":
        if (OS.isFamilyMac()) {

        System.setProperty("webdriver.gecko.driver", basePath+"/geckodriver_mac");
        } else if (OS.isFamilyWindows()) {
        // System.setProperty("webdriver.gecko.driver",  basePath=System.getProperty("user.dir")+"/src/main/resources");
        }else {

        }
        break;


default:
        System.out.println("Browser not supported");
        break;
        }


        }



@AfterSuite
public void teardown(){
        if(aDriver!=null){
        aDriver.quit();
        }
        }
        }

