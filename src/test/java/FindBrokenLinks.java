import Util.ExcelWriter;
import jxl.write.WriteException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

public class FindBrokenLinks {

    private  WebDriver driver = null;

    @Test
   public void findbrokenLinks() {
        // TODO Auto-generated method stub

        String homePage = "https://www.embibe.com/exams/";
        String basePath=System.getProperty("user.dir")+"/src/main/resources";
        System.setProperty("webdriver.chrome.driver",
                basePath+"/chromedriver_mac");
        String url = "";
        HttpURLConnection huc = null;
        ExcelWriter exl = new ExcelWriter();
        int respCode = 200;
        int   sheetno=1;
        int rowno=2;int columnno=2,count=0;
            ChromeOptions options=new ChromeOptions();
            options.addArguments("--headless");
        driver = new ChromeDriver(options);

        driver.manage().window().maximize();

        driver.get(homePage);

        List<WebElement> links = driver.findElements(By.tagName("a"));

        Iterator<WebElement> it = links.iterator();

        while(it.hasNext()){

            url = it.next().getAttribute("href");

            System.out.println(url);

            if(url == null || url.isEmpty()){
                System.out.println("URL is either not configured for anchor tag or it is empty");
                continue;
            }

            if(url.startsWith("google")||(url.startsWith("facebook"))||(url.startsWith("linkedin"))||(url.startsWith("twitter"))||(url.startsWith("mailto"))){
                System.out.println("URL belongs to another domain, skipping it.");
                continue;
            }

            try {
                huc = (HttpURLConnection)(new URL(url).openConnection());

                huc.setRequestMethod("HEAD");

                huc.connect();

                respCode = huc.getResponseCode();

                if(respCode != 200){
                    System.out.println(url+" is a broken link");
                    String str=url;
                    count++;
                    columnno++;
                    rowno++;
                    //   exl.addContentToCell(1,columnno,rowno,str);
                    exl.addContentToCell(1,count,respCode,rowno,str);
                    //     ExcelWriter.addContenttoCell
//                    try {
//                        columnno++;
//                        rowno++;
//                        //   exl.addContentToCell(1,columnno,rowno,str);
//                        exl.addContentToCell(1,count,respCode,rowno,str);
//                        Assert.assertEquals(true,true,"a broken link discovered");
//                    } catch (WriteException e) {
//                        e.printStackTrace();
//                    }



                }
                else{
                    System.out.println(url+" is a valid link");
                    Assert.assertEquals(true,true,"a valid link");
                }

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        driver.quit();

    }
}



